#!/usr/bin/env perl
use Config::General qw(ParseConfig);
use File::Basename;
use strict;
use warnings;


my $conffile = shift(@ARGV) || die("supply config file");

my %conf = ParseConfig($conffile);

my $apprepo_group_id = $conf{"apprepo_group_id"} ||
    die("missing config: apprepo_group_id");



### Name
my $name = $conf{"apprepo"} ||
    die("supply config: apprepo");
$name = basename($name);
$name =~ s/.git$//;
print "projname=$name\n";



# Infrarepo
my $infrarepo = $conf{"apprepo"} || die("missing config: apprepo");
if($infrarepo =~ /^https:\/\/(git...\.com)\/(.*.git)$/i) {
    $infrarepo = "git\@$1:$2";
}
$infrarepo =~ s/.git$/-infra.git/;
print "infrarepo=$infrarepo\n";
my $infrareponame = basename($infrarepo);
$infrareponame =~ s/.git$//;
print "infrareponame=$infrareponame\n";



if($conf{secrets_env_test}) {
    my $secretsrepo = $conf{"apprepo"} || die("missing config: apprepo");
    if($secretsrepo =~ /^https:\/\/(git...\.com)\/(.*\.git)$/) {
        $secretsrepo = "git\@$1:$2";
    }
    $secretsrepo =~ s/.git$/-secrets.git/;
    print "secretsrepo=$secretsrepo\n";
    my $secretsreponame = basename($secretsrepo);
    $secretsreponame =~ s/.git$//;
    print "secretsreponame=$secretsreponame\n";
}
