#!/usr/bin/env perl
use Config::General qw(ParseConfig);
use strict;
use warnings;


my $conffile = shift(@ARGV) || die("supply config file");

my %conf = ParseConfig($conffile);


die("set CONTAINERCMD") unless(defined($ENV{"CONTAINERCMD"}));
die("set SECRETS_GITLAB") unless(defined($ENV{"SECRETS_GITLAB"}));
die("set WEBHOOK_URL and WEBHOOK_TOKEN")
    unless(defined($ENV{"WEBHOOK_URL"}) && defined($ENV{"WEBHOOK_TOKEN"}));



warn("create main app repo webhook");
my $crtapphooks = `$ENV{CONTAINERCMD} run --env-file $ENV{SECRETS_GITLAB} --rm -i gitlab-client --project-id $conf{"apprepo_project_id"} --push-hook --merge-hook --tag-hook --url $ENV{"WEBHOOK_URL"} --token $ENV{"WEBHOOK_TOKEN"} create webhook >&2`;



warn("create infra webhooks");
die("project ID seems strange: ".$conf{infrarepo_project_id})
  unless(defined($conf{infrarepo_project_id}) && $conf{infrarepo_project_id} =~ /^\d+$/);
my $crtinfrahooks = `$ENV{CONTAINERCMD} run --env-file $ENV{SECRETS_GITLAB} --rm -i gitlab-client --project-id $conf{infrarepo_project_id} --push-hook --url $ENV{"WEBHOOK_URL"} --token $ENV{"WEBHOOK_TOKEN"} create webhook >&2`;
