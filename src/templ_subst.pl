#!/usr/bin/env perl
use Config::General qw(ParseConfig);
use File::Find;
use Getopt::Long;
use Pod::Usage qw(pod2usage);
use Template;
use strict;
use warnings;
no warnings 'File::Find';


# template toolkit object
my $tt = Template->new({
    INTERPOLATE  => 0,
    ABSOLUTE => 1,
    RECURSION => 1,
}) or die "$Template::ERROR\n";


# set config defaults here
my %conf = (
    "templatedir" => "/tmp",
    DEBUG => 1,
    );


# merge cli args into the conf
if(! GetOptions("config|c=s"    => \$conf{config},
                "templatefile|f=s" => \$conf{templatefile},
                "templatedir|d=s" => \$conf{templatedir})) {
    pod2usage(-verbose => 0, -exitval => 0);
    warn("fixme no exit?");
    exit(0);
}


# merge config file settings into the conf
if(defined($conf{config}) && -f glob($conf{config})) {
    my %fileconf = ParseConfig($conf{config});
    %conf = (%conf, %fileconf);
} else {
    die("cannot open config file $conf{config}");
}


#### Subroutines ###########################

# nifty callback function for File::Find::find(), called above
sub repl {
    if(-f $File::Find::name) {
        my $file = $File::Find::name;
        my $report;
        $tt->process($file, \%conf, \$report) or die $tt->error(), "\n";
        write_file($File::Find::name, $report);
    }
}



sub write_file {
    my $file = shift;
    my $data = shift;

    warn("write_file $file");
    open(my $fh, '>', $file) or die $!;
    print $fh $data;
    close($fh);
}


#############################################

if(defined($conf{templatefile}) && -f $conf{templatefile}) {
    warn("template toolkit on file $conf{templatefile}");
    my $report;
    $tt->process($conf{templatefile}, \%conf, \$report) or die $tt->error(), "\n";
    write_file($conf{templatefile}, $report);
} elsif(defined($conf{templatedir}) && -d $conf{templatedir}) {
    warn("template toolkit on dir $conf{templatedir}");
    find(\&repl, ($conf{"templatedir"}));
} else {
    die("supply either --templatefile or --templatedir");
}
