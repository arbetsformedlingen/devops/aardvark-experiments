#!/usr/bin/env perl
use Config::General qw(ParseConfig);
use JSON;
use strict;
use warnings;


my $conffile = shift(@ARGV) || die("supply config file");

my %conf = ParseConfig($conffile);


die("set CONTAINERCMD") unless(defined($ENV{"CONTAINERCMD"}));
die("set SECRETS_GITLAB") unless(defined($ENV{"SECRETS_GITLAB"}));



my $crtinfracmd = "$ENV{CONTAINERCMD} run --env-file $ENV{SECRETS_GITLAB} --rm -i gitlab-client --name $conf{infrareponame} --group-id $conf{apprepo_group_id} --visibility public create repo";
warn("create infra repo: $crtinfracmd");
my $crtinfra = `$crtinfracmd`;
my $infra_repo = decode_json $crtinfra || exit(1);
print "infrarepo_project_id=".$infra_repo->{projectid}."\n";


if($conf{"secretsrepo"}) {
    my $crtsecretscmd = "$ENV{CONTAINERCMD} run --env-file $ENV{SECRETS_GITLAB} --rm -i gitlab-client --name $conf{secretsreponame} --group-id $conf{apprepo_group_id} --visibility private create repo";
    warn("create secrets repo: $crtsecretscmd");
    my $crtsecrets = `$crtsecretscmd`;
    my $secrets_repo = decode_json $crtsecrets || exit(1);
    print "secretsrepo_project_id=".$secrets_repo->{projectid}."\n";
}
