#!/usr/bin/env perl
use Config::General qw(ParseConfig);
use strict;
use warnings;


my $conffile = shift(@ARGV) || die("supply config file");
my $outputdir = shift(@ARGV) || die("supply outputdir");

my %conf = ParseConfig($conffile);




sub mkgit {
  my ($codedir, $remoterepo) = @_;

  warn("creating git repo in $codedir and pushing to $remoterepo");
  chdir($codedir) || die("could not cd to $codedir");
  system("git init") == 0 || die("could not init new git repo");
  system("git checkout -b main") == 0 || die("could not create branch main");
  system("git add .") == 0 || die("could not add files to repo");
  system("git commit -m 'Initial commit'") == 0 || die("could not create initial commit");
  system("git remote add origin $remoterepo") == 0 || die("could not add remote $remoterepo");
  system("git push --force --set-upstream origin main") == 0 || die("could not push to remote");
}



warn("push generated files for infra");
mkgit($outputdir."/".$conf{"infrareponame"}, $conf{"infrarepo"});



if($conf{"secretsrepo"}) {
  warn("push generated files for secrets");
  mkgit($outputdir."/".$conf{"secretsreponame"}, $conf{"secretsrepo"});
}
