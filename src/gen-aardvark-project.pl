#!/usr/bin/env perl
use Config::General qw(ParseConfig);
use File::Copy::Recursive qw(dircopy);
use File::Copy;
use File::Find;
use File::Basename;
use Getopt::Long;
use Pod::Usage qw(pod2usage);
use strict;
use warnings;
no warnings 'File::Find';


# set config defaults here
my %conf = (
    "outputdir" => "/tmp",
    );


# merge cli args into the conf
if(! GetOptions("config|c=s"    => \$conf{config},
                "outputdir|d=s" => \$conf{outputdir})) {
    pod2usage(-verbose => 0, -exitval => 0);
    warn("fixme no exit?");
    exit(0);
}


# merge config file settings into the conf
if(defined($conf{config}) && -f glob($conf{config})) {
    my %fileconf = ParseConfig($conf{config});
    %conf = (%conf, %fileconf);
} else {
    die("cannot open config file $conf{config}");
}


#### Subroutines ###########################

sub add_secrets {
    my ($targetdir, $secretfile) = @_;

    warn("copy $secretfile $targetdir");
    copy($secretfile, $targetdir);
    my $kust = read_file($targetdir."/kustomization.yaml");
    $kust .= "  - ".basename($secretfile)."\n";
    write_file($targetdir."/kustomization.yaml", $kust);
}



sub read_file {
    my $file = shift;
    open my $fh, '<', $file or die;
    local $/ = undef;
    my $cont = <$fh>;
    close $fh;
    return $cont;
}



sub write_file {
    my $file = shift;
    my $data = shift;

    warn("write_file $file");
    open(my $fh, '>', $file) or die $!;
    print $fh $data;
    close($fh);
}


#############################################



# setup secrets
if(defined($conf{"secretsrepo"})) {
    my $secretdir = $conf{"outputdir"}."/".$conf{"projname"}."-secrets";
    if(! -d $secretdir) {
	warn("dircopy templates/projects/secrets $secretdir");
	dircopy("templates/projects/secrets", $secretdir);

	add_secrets($secretdir."/test", $conf{"secrets_env_test"})
	    if(defined($conf{"secrets_env_test"}));

	add_secrets($secretdir."/prod", $conf{"secrets_env_prod"})
	    if(defined($conf{"secrets_env_prod"}));
    }
}


# setup infra
my $infradir = $conf{"outputdir"}."/".$conf{"projname"}."-infra";
if(! -d $infradir) {
    warn("templates/projects/infra $infradir");
    dircopy("templates/projects/infra", $infradir);
}


# optionally add cronjob yaml
if(defined($conf{"cronimage"})) {
    copy("templates/files/cronjob.yaml", $infradir."/kustomize/base");
    write_file($infradir."/kustomize/base/kustomization.yaml",
               read_file($infradir."/kustomize/base/kustomization.yaml")."\n  - cronjob.yaml");
}


# optionally add pod yaml
if(defined($conf{"podimage"})) {
    copy("templates/files/pod.yaml", $infradir."/kustomize/base");
    write_file($infradir."/kustomize/base/kustomization.yaml",
               read_file($infradir."/kustomize/base/kustomization.yaml")."\n  - pod.yaml");
}


# optionally add pod yaml
if(defined($conf{"storagesize"})) {
    copy("templates/files/claim.yaml", $infradir."/kustomize/base");
    write_file($infradir."/kustomize/base/kustomization.yaml",
               read_file($infradir."/kustomize/base/kustomization.yaml")."\n  - claim.yaml");
}
