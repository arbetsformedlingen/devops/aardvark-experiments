.ONESHELL:
SHELL        = /bin/bash

export CONTAINERCMD = $(shell which podman || which docker)


SECRETS_AARDVARK = $(PWD)/secrets_aardvark.sh
SECRETS_GITLAB   = $(PWD)/secrets_gitlab.sh
SECRETS_OC       = $(PWD)/secrets_oc.sh


OUTPUTDIR    = $(PWD)/output


PROJECT_CONF = $(PWD)/examples/fortuneteller.conf
DERIVED_CONF = $(OUTPUTDIR)/$(shell basename $(PROJECT_CONF)).derived



all: verify_tools build mkdir $(DERIVED_CONF) gitlab_create_projects copy-templates subst-templates git oc gitlab_create_webhooks
	@echo
	@echo Now trig a webhook event in the app repo
	@echo


verify_tools:
	for tool in perl git oc kubectl kustomize $(CONTAINERCMD) ssh; do
		command -v "$${tool}" >/dev/null || { echo "*** please install $${tool}" >&2; exit 1; }
	done
	perl -MConfig::General -MFile::Basename -MFile::Copy -MFile::Copy::Recursive \
		-MFile::Find -MGetopt::Long -MJSON -MPod::Usage -MTemplate -e true



mkdir:
	if [ -d $(OUTPUTDIR) ]; then
		echo "**** remove existing $(OUTPUTDIR) first" >&2
		exit 1
	fi
	mkdir -p $(OUTPUTDIR)



.PHONY:
oc:
	. $(SECRETS_OC)
	export OC_USER OC_PASS
	cd "$(OUTPUTDIR)"/*-infra
	make -f Makefile.ocp login-test
	make -f Makefile.ocp deploy-test



.PHONY: git
git:
	src/git.pl "$(DERIVED_CONF)" "$(OUTPUTDIR)"



.PHONY: copy-templates
copy-templates: $(DERIVED_CONF)
	src/gen-aardvark-project.pl --outputdir "$(OUTPUTDIR)" --config "$(DERIVED_CONF)"



.PHONY: subst-templates
subst-templates:
	src/templ_subst.pl --templatefile "$(DERIVED_CONF)" --config "$(DERIVED_CONF)" \
	&&src/templ_subst.pl --templatedir "$(OUTPUTDIR)" --config "$(DERIVED_CONF)"



.PHONY: gitlab_create_webhooks
gitlab_create_webhooks: $(DERIVED_CONF)
	. $(SECRETS_AARDVARK)
	export WEBHOOK_TOKEN WEBHOOK_URL
	env SECRETS_GITLAB=$(SECRETS_GITLAB) src/gitlab_create_webhooks.pl $<



.PHONY: gitlab_create_projects
gitlab_create_projects: $(DERIVED_CONF)
	env SECRETS_GITLAB=$(SECRETS_GITLAB) src/gitlab_create_projects.pl $< > $(DERIVED_CONF).repoids \
	&& cat $(DERIVED_CONF).repoids >> $(DERIVED_CONF)



$(DERIVED_CONF): $(PROJECT_CONF)
	cp $< $@
	src/derive_settings.pl $< >> $@



build:
	$(MAKE) -f src/Makefile.build



clean:
	rm -rf "$(OUTPUTDIR)"
	$(MAKE) -f src/Makefile.build clean
