# NAME

aardvarkify

# SYNOPSIS

Automate the manual tasks involved in creating an [aardvark](https://gitlab.com/arbetsformedlingen/devops/aardvark) app.

Requirements:
 - The application should be available as a project in Gitlab.
 - The application should have a top level Dockerfile, and in general
   be compatible with [aardvark](https://gitlab.com/arbetsformedlingen/devops/aardvark).

What this program will do:
 - Create an `-infra` sibling repo project in Gitlab.
 - Create an application project in Open Shift.
 - Setup [aardvark](https://gitlab.com/arbetsformedlingen/devops/aardvark) webhooks for the application and infra projects.

What this program can optionally do too:
 - Create a private `-secrets` sibling repo in Gitlab.
 - Setup the application as a Open Shift cronjob.


# BUILD AND INSTALLATION

## Package dependencies
Install packages `perl-Config-General perl-JSON perl-File-Copy-Recursive perl-Template-Toolkit`, or `libconfig-general-perl libfile-copy-recursive-perl libtemplate-perl`.

These programs also need to be installed:
 - podman or docker
 - bash
 - git
 - kubectl
 - kustomize
 - make
 - oc
 - perl
 - ssh


## Containerised environment
Run `make build`.


# CONFIGURATION

## Application infra config
Create a configuration file based on `examples/fortuneteller.conf`. It should contain
these fields:
 - `apprepo` pointing to an application's git repo (ssh clone url, not https)
 - `apprepo_group_id` with the numerical ID of the app project's Gitlab group
 - `apprepo_project_id` with the numerical ID of the app project

For now, this program only has the cronjob template type included, so also you need to set:
 - `crontime = 0 * * * *` (or some other cron schedule)

To include secrets in the application Pod environment, create two
secret files (see examples) and set:
 - `secrets_env_prod = examples/fortune_secrets_prod.conf`
 - `secrets_env_test = examples/fortune_secrets_test.conf`


Specify this configuration file in the header of `Makefile`.


Also audit the Openshift cluster URLs in
[Makefile.ocp](templates/projects/infra/Makefile.ocp).
This program will use the `test` url in that list.


## Operations config
As this program needs to operate Gitlab, Openshift and Aardvark, it needs credentials.

In `secrets_aardvark.sh`, set:
 - `WEBHOOK_TOKEN=<token>`
 - `WEBHOOK_URL=<aardvark hook url>`

In `secrets_gitlab.sh`, set:
 - `GITLAB_API_ENDPOINT=https://gitlab.com/api/v4`
 - `GITLAB_API_PRIVATE_TOKEN=<gitlab api token>`

In `secrets_oc.sh`, set:
 - `OC_USER=<oc user>`
 - `OC_PASS=<oc pass>`

You should also make sure you have configured your ssh-access to
Gitlab, so the program can push files.

# Running the program
`make`
